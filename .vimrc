" vimplug install
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" vimplug
call plug#begin('~/.vim/plugged')

Plug '/usr/bin/fzf'
Plug 'junegunn/fzf.vim'
Plug 'airblade/vim-gitgutter'
Plug 'romainl/Apprentice'
Plug 'davidhalter/jedi-vim'
Plug 'hail2u/vim-css3-syntax'
Plug 'jiangmiao/auto-pairs'
Plug 'mileszs/ack.vim'
Plug 'sheerun/vim-polyglot'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-vinegar'
Plug 'w0rp/ale'

" Initialize plugin system
call plug#end()

" wrap lines
set wrap

"set backaspace to act normal
set backspace=eol,start,indent

"set numbers
set number

" show a visual line under the cursor's current line
set cursorline

" show the macthing part of the pair for []{} and ()
set showmatch

" ignore case while searching and highlight it
set ignorecase

"highlight search
set hlsearch
set incsearch

"spellchecking

"set spell spelllang=en_us

" Except when search query contains a caps
set smartcase

"disable highlighted search on ctrl L
if maparg('<C-L>', 'n') ==# ''
  nnoremap <silent> <C-L> :nohlsearch<C-R>=has('diff')?'<Bar>diffupdate':''<CR><CR><C-L>
endif

"set list chars
set list
set listchars=trail:~,tab:»·,nbsp:⎵,precedes:←,extends:→
set showbreak=↪

"vim git gutter
let g:gitgutter_max_signs=9999

" The Silver Searcher
if executable('ag')
  let g:ackprg = 'ag --nogroup --nocolor --column'
  set grepprg=rg\ --vimgrep
endif

"path for good old find
set path=$PWD/**

" bind K to grep word under cursor
nnoremap K :grep! "\b<C-R><C-W>\b"<CR>:cw<CR>

"colorscheme
colorscheme apprentice

"settings for ale
let g:ale_css_stylelint_options  = ''
let g:ale_linters = {
\   'html': ['proselint', 'tidy'],
\   'javascript': ['prettier', 'eslint'],
\   'less': ['stylelint'],
\   'css': ['prettier', 'stylelint'],
\   'scss': ['stylelint'],
\   'markdown': ['proselint', 'prettier']
\}

let g:ale_fixers = {
\   '*': ['remove_trailing_lines', 'trim_whitespaces'],
\   'scss': ['stylelint'],
\   'less': ['stylelint'],
\   'css': ['stylelint'],
\   'html': ['tidy']
\}

"shortcut for files search
nnoremap <leader>o :Files<cr>

" 'matchit.vim' is built-in so let's enable it!
" Hit '%' on 'if' to jump to 'else'.
runtime macros/matchit.vim

"settings for status line

"always show status line
set laststatus=2

"Key bindings go here
nnoremap <C-j> :m .+1<CR>==
nnoremap <C-k> :m .-2<CR>==
inoremap <C-j> <Esc>:m .+1<CR>==gi
inoremap <C-k> <Esc>:m .-2<CR>==gi
vnoremap <C-j> :m '>+1<CR>gv=gv
vnoremap <C-k> :m '<-2<CR>gv=gv

" funtion to get branch from fugitive
function! Current_git_branch()
    let l:branch = split(fugitive#statusline(),'[()]')
    if len(l:branch) > 1
         return remove(l:branch, 1)
    endif
    return ""
endfunction

"status line start
set statusline=
set statusline+=%0*\[%n]                               "buffer number
set statusline+=%1*\ %{Current_git_branch()}\          "git branch from fugitive
set statusline+=%2*\ %<%F\                             "file path
set statusline+=%3*\ %=\ %y\                           "file type
set statusline+=%4*\ %{''.(&fenc!=''?&fenc:&enc).''}   "Encoding
set statusline+=%4*\ %{(&bomb?\",BOM\":\"\")}\         "Encoding 2
set statusline+=%5*\ %{&ff}\                           "File format (dos /unix)
set statusline+=%6*\ row:%l/%L\ (%03p%%)\              "row number/total %
set statusline+=%7*\ col:%03c\                         "column number
set statusline+=%8*\ \ %m%r%w\ %P\ \                   "modified readonly top bottom

" colors for statusline
highlight User0 ctermfg=242 ctermbg=234 guifg=#6c6c6c guibg=#1c1c1c
highlight User1 ctermfg=234 ctermbg=65  guifg=#1c1c1c guibg=#5f875f gui=bold
highlight User2 ctermfg=250 ctermbg=234 guifg=#bcbcbc guibg=#1c1c1c
highlight User3 ctermfg=238 ctermbg=234 guifg=#444444 guibg=#1c1c1c
highlight User4 ctermfg=238 ctermbg=234 guifg=#444444 guibg=#1c1c1c
highlight User5 ctermfg=238 ctermbg=234 guifg=#444444 guibg=#1c1c1c
highlight User6 ctermfg=242 ctermbg=234 guifg=#6c6c6c guibg=#1c1c1c
highlight User7 ctermfg=242 ctermbg=234 guifg=#6c6c6c guibg=#1c1c1c
hi User8 ctermfg=242 ctermbg=234 guifg=#6c6c6c guibg=#1c1c1c
